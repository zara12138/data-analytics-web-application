var Revision = require("../models/revision")
var Individual = require("../models/individual")

module.exports.showTitleForm = function(req, res) {

	var MostRevisions
	var LeastRevisions
	var LargestGroup
	var SmallestGroup
	var Longest
	var Shortest
	var ArtList

	var flag = 0
	
	// The article with the most number of revisions
	Revision.findMostRevisions(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			MostRevisions = result[0]			
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// The article with the least number of recisions
	Revision.findLeastRevisions(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			LeastRevisions = result[0]
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// The article edited by largest group of registered users
	Revision.findLargestGroup(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			LargestGroup = result[0]
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// The article edited by smallest group of registered users
	Revision.findSmallestGroup(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			SmallestGroup = result[0]
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// The article with the longest history
	Revision.findLongest(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			Longest = result[0]
			Longest.oldest = calcDate(new Date(), new Date(Longest.oldest))
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// The article with the shortest history
	Revision.findShortest(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			Shortest = result[0]
			Shortest.lastest = calcDate(new Date(), new Date(Shortest.lastest))
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}
	})
	// article list for individual part
	Individual.listTitles(function(err,result){		
		if (err){
			console.log(err)
		}else{
			ArtList = result
			flag+=1
			if(flag==7){
				res.render('index.ejs', {
					mostRevisions : MostRevisions,
					leastRevisions : LeastRevisions,
					largestGroup : LargestGroup,
					smallestGroup : SmallestGroup,
					longest : Longest,
					shortest : Shortest,
					titles : ArtList
				})
			}
		}	
	})
}

// calculate age of article
function calcDate(date1,date2) {
    var diff = date1 - date2;
    var secPerDay = 86400*1000;

    var days = Math.floor(diff/secPerDay);

    var message =  days + " days ago" 

    return message
    }

// get overall bar chart
module.exports.getBarChart = function(req, res){
	var admin;
	var bot;
	var anon;
	var reg;
	var flag = 0
	var temp =[]
	
	// get(year and number of revision) for admin
	Revision.getAdminAndYear(function(err, result){
		if(err){
			console.log(err)
		}else{
			flag += 1
			admin = result
			if(flag == 4){
				temp.push(admin,anon,bot,reg)
				var a = mergeArrays(temp)
				a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
				res.send(a)
			}
		}
	})
	// get bot(year and number of revision)
	Revision.getBotAndYear(function(err, result){
		if(err){
			console.log(err)
		}else{
			flag += 1
			bot = result
			if(flag == 4){
				temp.push(admin,anon,bot,reg)
				var a = mergeArrays(temp)
				a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
				res.send(a)
			}
		}
	})
	// get anon(years and numbers of revision)
	Revision.getAnonAndYear(function(err, result){
		if(err){
			console.log(err)
		}else{
			flag += 1
			anon = result
			if(flag == 4){
				temp.push(admin,anon,bot,reg)
				var a = mergeArrays(temp)
				a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
				res.send(a)
			}
		}
	})
	// get regular user(years and numberr of revision)
	Revision.getRegAndYear(function(err, result){
		if(err){
			console.log(err)
		}else{
			flag += 1
			reg = result
			if(flag == 4){
				temp.push(admin,anon,bot,reg)
				var a = mergeArrays(temp)
				a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
				res.send(a)
			}
			
		}
	})
}

// get overall pie chart
module.exports.getPieChart = function(req, res) {
	var admin_num;
	var bot_num;
	var anon_num;
	var reg_num;
	var flag = 0
	// get number of admin users
	Revision.getAdminNum(function(err, result) {
		if (err) {
			console.log(err)
			return
		} else {
			admin_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of bot users
	Revision.getBotNum(function(err, result) {
		if (err) {
			console.log(err)
			return
		} else {
			bot_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of anon user
	Revision.getAnonNum(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			anon_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of regular user
	Revision.getRegNum(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			reg_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
}
// search article
// var title,sum, top5User;
module.exports.showResult=function(req,res){
	title = req.query.title
	var flag = 0
	var sum, top5
	var num = 0
	Individual.findLatest(title,function(err,result){
		if(err){
			console.log(err)
		}else{
			var latest = new Date(result[0].timestamp);
			var now = new Date();
			var hours = (now - latest) / 3600000;
			console.log("hours:"+hours)
			if (hours >= 24){
				//if the latest revision is not up-to-date, then download new revisions from wikipedia,then save, then query from database
				Individual.getJsonFromWiki(title, result[0].timestamp,function(result){
					num = result.update
					Individual.findArticle(title,function(err,result){
						if(err){
							console.log(err)
						}else{
							flag+=1
							sum = result[0].numOfEdits
							if(flag == 2){
								res.json({'title':title,'sum':sum,'top5':top5,'result':num})
							}
						}
					})
					Individual.findTop5(title, function(err,result){					
						if (err){
							console.log(err)
						}else{
							flag+=1
							top5 = result
							if(flag == 2){
								res.json({'title':title,'sum':sum,'top5':top5,'result':num})
							}
						}	
					})
				})
				//if the revision is up to date, then query from database directly
			}else{
				Individual.findArticle(title, function(err,result){		
					if (err){
						console.log(err)
					}else{
						flag+=1
						sum = result[0].numOfEdits
						if(flag == 2){
							res.json({'title':title,'sum':sum,'top5':top5,'result':-1})
						}
					}	
				})
				Individual.findTop5(title, function(err,result){					
					if (err){
						console.log(err)
					}else{
						flag+=1
						top5 = result
						if(flag == 2){
							res.json({'title':title,'sum':sum,'top5':top5,'result':num})
						}
					}	
				})
			}
		}
	})
	
}	
// get the data for individual column chart(revisions by year and user)
module.exports.getIColumnChart = function(req, res){
		var admin;
		var bot;
		var anon;
		var reg;
		var flag = 0
		var title = req.query.title
		var temp = []
		Individual.getAdminAndYear(title, function(err,result){
			if(err){
				console.log(err)
			}else{
				flag += 1
				admin = result
				if(flag == 4){
					temp.push(admin,anon,bot,reg)
					var a = mergeArrays(temp)
					a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
					res.send(a)
				}
			}
		})
		Individual.getBotAndYear(title, function(err,result){
			if(err){
				console.log(err)
			}else{
				flag += 1
				bot = result
				if(flag == 4){
					temp.push(admin,anon,bot,reg)
					var a = mergeArrays(temp)
					a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
					res.send(a)
				}
			}
		})
		Individual.getAnonAndYear(title, function(err,result){
			if(err){
				console.log(err)
			}else{
				flag += 1
				anon = result
				if(flag == 4){
					temp.push(admin,anon,bot,reg)
					var a = mergeArrays(temp)
					a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
					res.send(a)
				}
			}
		})
		Individual.getRegAndYear(title, function(err,result){
			if(err){
				console.log(err)
			}else{
				flag += 1
				reg = result
				if(flag == 4){
					temp.push(admin,anon,bot,reg)
					var a = mergeArrays(temp)
					a.unshift(['Year', 'Administrator', 'Anonymous', 'Bot', 'Regular user'])
					res.send(a)
				}
			}
		})
	}
// get data for pie chart(percentage)
module.exports.getIPieChart = function(req, res) {
	var admin_num;
	var bot_num;
	var anon_num;
	var reg_num;
	var flag = 0
	var title = req.query.title
	// get number of admin users
	Individual.getAdminNum(title, function(err, result) {
		if (err) {
			console.log(err)
		} else {
			admin_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of bot users
	Individual.getBotNum(title, function(err, result) {
		if (err) {
			console.log(err)
		} else {
			bot_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of anon user
	Individual.getAnonNum(title, function(err, result) {
		if (err) {
			console.log(err)
		} else {
			anon_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
	// get number of regular user
	Individual.getRegNum(title, function(err, result) {
		if (err) {
			console.log(err)
		} else {
			reg_num = result
			flag += 1
			if(flag == 4){
				res.json({'Administrator': admin_num, 'Anonymous': anon_num, 'Bot': bot_num, 'Regular user': reg_num});
			}
		}
	})
}

// merge number of revision array get from which top5 users were selected
mergeArrays = function(arr){
	var max = 0
	var min = 2017
	for(var i = 0; i<arr.length; i++){
	 for(var j = 0; j<arr[i].length;j++){
	  if(max<arr[i][j]._id){
	   max = arr[i][j]._id
	  }
	  if(min>arr[i][j]._id){
	   min = arr[i][j]._id
	  }
	 }
	}
	var array = []
	for(var i = min; i<= max; i++){
	 var temp = new Array
	 temp.push(i)
	 for(var j = 0; j<arr.length;j++){
	  var flag = 0
	  for(var m = 0; m< arr[j].length; m++){
	   if(arr[j][m]._id == i){
	    temp.push(arr[j][m].number)
	    flag = 1
	   }
	  }
	  if(flag == 0){
	   temp.push(0)
	  }
	 }
	 array.push(temp)
	}
	return array
}
// get data for bar chart(top5 user)
module.exports.getIBarChart = function(req, res){
	var top5 = req.query.top5
	var title = req.query.title
	var results = new Array
	var flag = 0
	var chartTitle = ['Year']
	for(var i = 0; i< top5.length;i++){
		chartTitle.push(top5[i])
		Individual.findTop5ByYears(top5[i],title,function(err,result){
			if(err){
				console.log(err)
			}else{
				flag++
				results.push(result)
				if(flag==top5.length){
					var final = mergeArrays(results)
					final.unshift(chartTitle)
					res.send(final)
				}
			}
		})
	}
}