var mongoose = require('./db')

//admin schema(_id,user)
var AdminSchema = new mongoose.Schema({
	_id : String,
	user : String
}, {
	versionKey : false
})

//get admin user list
AdminSchema.statics.getAdminList = function(callback) {
	return this.distinct("user").exec(callback)
}


var Admin = mongoose.model('Admin', AdminSchema, 'admin')

module.exports = Admin