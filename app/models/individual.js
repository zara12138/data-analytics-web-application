var mongoose = require('./db')
var Bot = require("../models/bot")
var Admin = require("../models/admin")
var https = require('https')

var IndividualSchema = new mongoose.Schema({
	title : String,
	timestamp : String,
	user : String,
	anon : String
}, {
	versionKey : false
})
var admin_users = new Array
var bot_users = new Array

// list all title when load page
IndividualSchema.statics.listTitles = function(callback) {
	return this.distinct('title').exec(callback)
}

// find the number of revisions given title(str)
IndividualSchema.statics.findArticle = function(str, callback) {

	return this.aggregate([ {
		$match : {
			title : str
		}
	}, {
		$group : {
			_id : "$title",
			numOfEdits : {
				$sum : 1
			}
		}
	}, ]).exec(callback)

}

IndividualSchema.statics.findLatest = function(str, callback) {
	return this.find({
		title : str
	}).sort({
		timestamp : -1
	}).limit(1).exec(callback)
}

// insert new revisions from wikipidia after the given time and given by article
// title
IndividualSchema.statics.getJsonFromWiki = function(title, start, callback) {
	var wikiEndpointHost = "en.wikipedia.org", path = "/w/api.php"
	parameters = [ "action=query", "format=json", "prop=revisions",
			"titles=" + title.replace(/ /g, "%20"), "rvstart=" + start,
			"rvdir=newer", "rvlimit=max", "rvprop=timestamp|userid|user|ids" ],
			headers = {
				Accept : 'application/json',
				'Accept-Charset' : 'utf-8'
			}
	var full_path = path + "?" + parameters.join("&")
	console.log(wikiEndpointHost + full_path)
	var options = {
		host : wikiEndpointHost,
		path : full_path,
		headers : headers
	}
	// number of revisions inserted
	var num = 0
	https.get(options, function(res) {
		var data = '';
		res.on('data', function(chunk) {
			data += chunk
		})
		res.on('end', function() {
			console.log("data:" + data)
			json = JSON.parse(data);
			pages = json.query.pages
			revisions = pages[Object.keys(pages)[0]].revisions
			console.log("There are " + revisions.length + " revisions.");
			num = revisions.length
			for (revid in revisions) {
				if (revisions[revid].timestamp != start) {
					var re = new Individual({
						"title" : title,
						"timestamp" : revisions[revid].timestamp,
						"user" : revisions[revid].user,
						"anon" : revisions[revid].anon,
					})
					console.log(re)
					re.save()
				} else {
					num--
				}
			}
			return callback({
				'update' : num
			})
		})
	}).on('error', function(e) {
		console.log(e)
		return callback()
	})

}

// find the top5 users given title(str)
IndividualSchema.statics.findTop5 = function(str, callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			bot_users = result
			if (flag == 2) {
				return Individual.aggregate([ {
					$match : {
						user : {
							$nin : bot_users,
							$nin : admin_users
						},
						anon : {
							$exists : false
						},
						title : str
					}
				}, {
					$group : {
						_id : "$user",
						numOfEdits : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						numOfEdits : -1
					}
				}, {
					$limit : 5
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Individual.aggregate([ {
					$match : {
						user : {
							$nin : bot_users,
							$nin : admin_users
						},
						anon : {
							$exists : false
						},
						title : str
					}
				}, {
					$group : {
						_id : "$user",
						numOfEdits : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						numOfEdits : -1
					}
				}, {
					$limit : 5
				} ]).exec(callback)
			}
		}
	})
}

// get number of revisions given user name and article name
IndividualSchema.statics.findTop5ByYears = function(userName, titleName,
		callback) {
	return this.aggregate([ {
		$match : {
			user : userName,
			title : titleName
		}
	}, {
		$group : {
			_id : {
				$substr : [ '$timestamp', 0, 4 ]
			},
			number : {
				$sum : 1
			}
		}
	}, {
		$sort : {
			_id : 1
		}
	} ]).exec(callback)
}

// get revision edited by admin by year given article name
IndividualSchema.statics.getAdminAndYear = function(str, callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			bot_users = result
			if (flag == 2) {
				return Individual.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$in : admin_users
							}
						}, {
							user : {
								$nin : bot_users
							}
						}, {
							title : str
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Individual.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$in : admin_users
							}
						}, {
							user : {
								$nin : bot_users
							}
						}, {
							title : str
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})

}

// get revision edited by bot by year given article name
IndividualSchema.statics.getBotAndYear = function(str, callback) {
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			bot_users = result
			return Individual.aggregate([ {
				$match : {
					user : {
						$in : bot_users
					},
					title : str
				}
			}, {
				$group : {
					_id : {
						$substr : [ '$timestamp', 0, 4 ]
					},
					number : {
						$sum : 1
					}
				}
			}, {
				$sort : {
					_id : 1
				}
			} ]).exec(callback)
		}
	})
}

// get revision edited by anon by year given article name
IndividualSchema.statics.getAnonAndYear = function(str, callback) {
	return this.aggregate([ {
		$match : {
			anon : {
				$exists : true
			},
			title : str
		}
	}, {
		$group : {
			_id : {
				$substr : [ '$timestamp', 0, 4 ]
			},
			number : {
				$sum : 1
			}
		}
	}, {
		$sort : {
			_id : 1
		}
	} ]).exec(callback)
}

// get revision edited by regular user by year given article name
IndividualSchema.statics.getRegAndYear = function(str, callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Individual.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						}, {
							title : str
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Individual.aggregate([ {
					$match : {$and : [ {
						user : {
							$nin : bot_users
						}
					}, {
						user : {
							$nin : admin_users
						}
					}, {
						anon : {
							$exists : false
						}
					}, {
						userHidden : {
							$exists : false
						}
					}, {
						suppressed : {
							$exists : false
						}
					}, {
						title : str
					} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})

}

// get admin user number given article name
IndividualSchema.statics.getAdminNum = function(str, callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Individual.count({
					$and : [ {
						user : {
							$in : admin_users
						}
					}, {
						user : {
							$nin : bot_users
						}
					}, {
						title : str
					} ]
				}).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Individual.count({
					$and : [ {
						user : {
							$in : admin_users
						}
					}, {
						user : {
							$nin : bot_users
						}
					}, {
						title : str
					} ]
				}).exec(callback)
			}
		}
	})
}
// get bot user number given article name
IndividualSchema.statics.getBotNum = function(str, callback) {
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			bot_users = result
			return Individual.count({
				user : {
					$in : bot_users
				},
				anon : {
					$exists : false
				},
				title : str
			}).exec(callback)
		}
	})
}
// get anon user number given article name
IndividualSchema.statics.getAnonNum = function(str, callback) {

	return Individual.count({
		user : {
			$nin : bot_users,
			$nin : admin_users
		},
		anon : {
			$exists : true
		},
		title : str
	}).exec(callback)
}
// get regular user number given article name
IndividualSchema.statics.getRegNum = function(str, callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Individual.count({
					$and : [ {
						user : {
							$nin : bot_users
						}
					}, {
						user : {
							$nin : admin_users
						}
					}, {
						anon : {
							$exists : false
						}
					}, {
						userHidden : {
							$exists : false
						}
					}, {
						suppressed : {
							$exists : false
						}
					}, {
						title : str
					} ]
				}).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Individual.count({
					$and : [ {
						user : {
							$nin : bot_users
						}
					}, {
						user : {
							$nin : admin_users
						}
					}, {
						anon : {
							$exists : false
						}
					}, {
						userHidden : {
							$exists : false
						}
					}, {
						suppressed : {
							$exists : false
						}
					}, {
						title : str
					} ]
				}).exec(callback)
			}
		}
	})
}

var Individual = mongoose.model('Individual', IndividualSchema, 'a2-test1')

module.exports = Individual
