var mongoose = require('./db')

//bot schema(_id,user)
var BotSchema = new mongoose.Schema({
	_id : String,
	user : String
}, {
	versionKey : false
})

//get bot user list
BotSchema.statics.getBotList = function(callback) {
	return this.distinct("user").exec(callback)
}

var Bot = mongoose.model('Bot', BotSchema, 'bot')

module.exports = Bot