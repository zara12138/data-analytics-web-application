var mongoose = require('./db')
var Bot = require("../models/bot")
var Admin = require("../models/admin")
var RevisionSchema = new mongoose.Schema({
	_id : String,
	title : String,
	timestamp : String,
	user : String,
	anon : String
}, {
	versionKey : false
})

var admin_users = new Array
var bot_users = new Array

// The article with the most number of revisions
RevisionSchema.statics.findMostRevisions = function(callback) {

	return this.aggregate([ {
		$group : {
			_id : "$title",
			numOfEdits : {
				$sum : 1
			}
		}
	}, {
		$sort : {
			numOfEdits : -1
		}
	}, {
		$limit : 1
	} ]).exec(callback)
}

// The article with the least number of recisions
RevisionSchema.statics.findLeastRevisions = function(callback) {

	return this.aggregate([ {
		$group : {
			_id : "$title",
			numOfEdits : {
				$sum : 1
			}
		}
	}, {
		$sort : {
			numOfEdits : 1
		}
	}, {
		$limit : 1
	} ]).exec(callback)
}
// The article edited by largest group of registered users
RevisionSchema.statics.findLargestGroup = function(callback) {

	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : "$title",
						uniqueCount : {
							$addToSet : "$user"
						}
					}
				}, {
					$project : {
						_id : 1,
						uniqueUserCount : {
							$size : "$uniqueCount"
						}
					}
				}, {
					$sort : {
						uniqueUserCount : -1
					}
				}, {
					$limit : 1
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : "$title",
						uniqueCount : {
							$addToSet : "$user"
						}
					}
				}, {
					$project : {
						_id : 1,
						uniqueUserCount : {
							$size : "$uniqueCount"
						}
					}
				}, {
					$sort : {
						uniqueUserCount : -1
					}
				}, {
					$limit : 1
				} ]).exec(callback)
			}
		}
	})
}
// The article edited by smallest group of tegistered users
RevisionSchema.statics.findSmallestGroup = function(callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : "$title",
						uniqueCount : {
							$addToSet : "$user"
						}
					}
				}, {
					$project : {
						_id : 1,
						uniqueUserCount : {
							$size : "$uniqueCount"
						}
					}
				}, {
					$sort : {
						uniqueUserCount : 1
					}
				}, {
					$limit : 1
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : "$title",
						uniqueCount : {
							$addToSet : "$user"
						}
					}
				}, {
					$project : {
						_id : 1,
						uniqueUserCount : {
							$size : "$uniqueCount"
						}
					}
				}, {
					$sort : {
						uniqueUserCount : -1
					}
				}, {
					$limit : 1
				} ]).exec(callback)
			}
		}
	})
}
// The article with the longest history
RevisionSchema.statics.findLongest = function(callback) {

	return this.aggregate([ {
		$group : {
			"_id" : "$title",
			"oldest" : {
				$last : "$timestamp"
			}
		}
	}, {
		$sort : {
			"oldest" : 1
		}
	}, {
		$limit : 1
	} ]).exec(callback)
}
// The article with the shortest history
RevisionSchema.statics.findShortest = function(callback) {

	return this.aggregate([ {
		$group : {
			"_id" : "$title",
			"lastest" : {
				$last : "$timestamp"
			}
		}
	}, {
		$sort : {
			"lastest" : -1
		}
	}, {
		$limit : 1
	} ]).exec(callback)
}
// get revision edited by admin by year
RevisionSchema.statics.getAdminAndYear = function(callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$in : admin_users
							}
						}, {
							user : {
								$nin : bot_users
							}
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$in : admin_users
							}
						}, {
							user : {
								$nin : bot_users
							}
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})

}

// get revision edited by bot by year
RevisionSchema.statics.getBotAndYear = function(callback) {
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			bot_users = result
			return Revision.aggregate([ {
				$match : {
					user : {
						$in : bot_users
					}
				}
			}, {
				$group : {
					_id : {
						$substr : [ '$timestamp', 0, 4 ]
					},
					number : {
						$sum : 1
					}
				}
			}, {
				$sort : {
					_id : 1
				}
			} ]).exec(callback)
		}
	})
}

// get revision edited by anon by year
RevisionSchema.statics.getAnonAndYear = function(callback) {
	return this.aggregate([ {
		$match : {
			anon : {
				$exists : true
			}
		}
	}, {
		$group : {
			_id : {
				$substr : [ '$timestamp', 0, 4 ]
			},
			number : {
				$sum : 1
			}
		}
	}, {
		$sort : {
			_id : 1
		}
	} ]).exec(callback)
}

// get revision edited by regular user by year
RevisionSchema.statics.getRegAndYear = function(callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.aggregate([ {
					$match : {
						$and : [ {
							user : {
								$nin : bot_users
							}
						}, {
							user : {
								$nin : admin_users
							}
						}, {
							anon : {
								$exists : false
							}
						}, {
							userHidden : {
								$exists : false
							}
						}, {
							suppressed : {
								$exists : false
							}
						} ]
					}
				}, {
					$group : {
						_id : {
							$substr : [ '$timestamp', 0, 4 ]
						},
						number : {
							$sum : 1
						}
					}
				}, {
					$sort : {
						_id : 1
					}
				} ]).exec(callback)
			}
		}
	})

}

// get admin user number
RevisionSchema.statics.getAdminNum = function(callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.count({
					$and : [ {
						user : {
							$in : admin_users
						}
					}, {
						user : {
							$nin : bot_users
						}
					} ]
				}).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.count({
					$and : [ {
						user : {
							$in : admin_users
						}
					}, {
						user : {
							$nin : bot_users
						}
					} ]
				}).exec(callback)
			}
		}
	})
}
// get bot user number
RevisionSchema.statics.getBotNum = function(callback) {
	Bot.getBotList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			bot_users = result
			return Revision.count({
				user : {
					$in : bot_users
				},
				anon : {
					$exists : false
				}
			}).exec(callback)
		}
	})
}
// get anon user number
RevisionSchema.statics.getAnonNum = function(callback) {

	return Revision.count({
		user : {
			$nin : bot_users,
			$nin : admin_users
		},
		anon : {
			$exists : true
		}
	}).exec(callback)
}
// get regular user number
RevisionSchema.statics.getRegNum = function(callback) {
	var flag = 0;
	Bot.getBotList(function(err, result) {
		if (err) {
			console(err)
		} else {
			flag += 1
			bot_users = result
			if (flag = 2) {
				return Revision.count({
					$and : [ {
						user : {
							$nin : bot_users
						}
					}, {
						user : {
							$nin : admin_users
						}
					}, {
						anon : {
							$exists : false
						}
					}, {
						userHidden : {
							$exists : false
						}
					}, {
						suppressed : {
							$exists : false
						}
					} ]
				}).exec(callback)
			}
		}
	})
	Admin.getAdminList(function(err, result) {
		if (err) {
			console.log(err)
		} else {
			flag += 1
			admin_users = result
			if (flag == 2) {
				return Revision.count({
					$and : [ {
						user : {
							$nin : bot_users
						}
					}, {
						user : {
							$nin : admin_users
						}
					}, {
						anon : {
							$exists : false
						}
					}, {
						userHidden : {
							$exists : false
						}
					}, {
						suppressed : {
							$exists : false
						}
					} ]
				}).exec(callback)
			}
		}
	})
}

var Revision = mongoose.model('Revision', RevisionSchema, 'a2-test1')

module.exports = Revision
