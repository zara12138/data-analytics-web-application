google.charts.load('current', {
	packages : [ 'corechart' ]
});
google.charts.load('current', {
	packages : [ 'bar' ]
});

var PieOptions = {
	'title' : "Revision distribution by user type",
	'width' : 1000,
	'height' : 600,
	pieSliceText: 'label',
	'slices': {  2: {offset: 0.2} }
};
var BarOptions = {
	'title' : 'Revision distribution by year and by user type',
	'width' : 1000,
	'height' : 600,
	vAxis : {
		title : 'Number of revisions'
	},
	hAxis : {
		title : 'Year'
	}
};
var barData, pieData, IColData, IPieData, IBarData;
var title, sum, top5


function openTab(evt, Name) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(Name).style.display = "block";
	evt.currentTarget.className += " active";
}

function drawPie() {
	graphData = new google.visualization.DataTable();
	graphData.addColumn('string', 'Element');
	graphData.addColumn('number', 'Percentage');
	$.each(pieData, function(key, val) {
		graphData.addRow([ key, val ]);
	})
	var chart = new google.visualization.PieChart($("#O_chart")[0]);
	chart.draw(graphData, PieOptions);
}

function drawBar() {
	graphData = google.visualization.arrayToDataTable(barData);
	var chart = new google.visualization.ColumnChart($("#O_chart")[0]);
	chart.draw(graphData, BarOptions);
}

function showContent(n) {
	var parameters = {
		title : $('#select').val()
	};
	$.get('/IndividualData', parameters, function(result) {
		title = result.title
		$("#title").text(title)
		$("#Number").text(result.sum)
		$("table").empty()
		$("table").append("<tr><th>User</th><th>Number of revisions</th></tr>")
		$.each(result.top5, function(key, value) {
			var html = '<tr><td>' + value._id + '</td><td>' + value.numOfEdits
					+ '</td><td><input type="checkbox" value="' + value._id
					+ '"></td></tr>';
			$("table").append(html);
		})
		if(n == 1){
			var i =result.result
			if(i == -1){
				alert("This article is up-to-date.")
			}else if(i == 0){
				alert("This article is not up-to-date, but no new revision was downloaded.")
			}else{
				alert("This article is not up-to-date, and "+i+" revisions has been downloaded")
			}
		}
	});

	$.get('/IColChart', {
		title : $('#select').val()
	}, function(rdata) {
		IColData = rdata;
		drawIColChart()
	});

	$.get('/IPieChart', {
		title : $('#select').val()
	}, function(rdata) {
		IPieData = rdata;
	});
}

function drawIColChart() {
	graphData = google.visualization.arrayToDataTable(IColData);
	var chart = new google.visualization.ColumnChart($("#I_chart")[0]);
	var IColOptions = {
			'title' : 'Revision distribution by year and by user type for article '+ $('#select').val(),
			'width' : 1000,
			'height' : 600,
			vAxis : {
				title : 'Number of revisions'
			},
			hAxis : {
				title : 'Year'
			}
		};
	chart.draw(graphData, IColOptions);
}

function drawIBarChart() {
	var Users = new Array
	$("input:checkbox:checked").each(function() {
		Users.push($(this).val());
	});
	if(Users.length == 0){
		alert("Please select at least one user!")
	}else{
		$.get('/IBarChart', {
			top5 : Users,
			title : title
		}, function(rdata) {
			IBarData = rdata;
			graphData = google.visualization.arrayToDataTable(IBarData);
			var chart = new google.visualization.ColumnChart($("#I_chart")[0]);
			var IBarOptions = {
					'title' : 'Revision distribution by year of user(s) '+Users+' for article '+title,
					'width' : 1000,
					'height' : 600,
					vAxis : {
						title : 'Number of revisions'
					},
					hAxis : {
						title : 'Year'
					}
				};
			chart.draw(graphData, IBarOptions);
		});
	}
}

function drawIPieChart() {
	graphData = new google.visualization.DataTable();
	graphData.addColumn('string', 'Element');
	graphData.addColumn('number', 'Percentage');
	$.each(IPieData, function(key, val) {
		graphData.addRow([ key, val ]);
	})
	var chart = new google.visualization.PieChart($("#I_chart")[0]);
	var IPieOptions = {
		'title' : "Revision distribution by user type for article " + title,
		'width' : 1000,
		'height' : 600,
		'pieSliceText': 'label',
		'slices': {  2: {offset: 0.2} }
	};
	chart.draw(graphData, IPieOptions);
}

$(document).ready(function() {
	$.get('/pieData', null, function(rdata) {
		pieData = rdata;
	});
	$.get('/barData', null, function(rdata) {
		barData = rdata;
		drawBar()
	});

	$("#select").val($("#select option:first").val());

	showContent(0);

	$("#defaultOpenTab").click();

});
