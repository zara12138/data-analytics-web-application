var express = require('express');
var path = require('path')

var revroutes = require('./app/routes/revision.server.routes')

var app = express()

app.set('views', path.join(__dirname,'app','views'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/',revroutes)
// app.use('/pieData',revroutes)
// app.use('/barData',revroutes)
// app.use('/IndividualData', revroutes)
// app.use('/IColChart', revroutes)
// app.use('/IBarChart', revroutes)
// app.use('/IPieChart', revroutes)
app.listen(3000, function () {
	  console.log('Revision app listening on port 3000!')
	})
	
module.exports = app;